package uz.chayxana.javafood.additionalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/additional-service")
public class AdditionalController {
    @Autowired
    AdditionalService additionalService;

    @GetMapping
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(additionalService.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(additionalService.getOne(id));
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Additional additional) {
        return ResponseEntity.ok(additionalService.add(additional));
    }
}
